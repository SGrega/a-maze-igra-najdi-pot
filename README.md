
# 1. Kratek opis problema: #
Slabovidne osebe, še posebej otroci, s težavami z vidom imajo pogosto zaradi tega tudi moteno sposobnost gibanja, predvsem razvoj gibanja rok (fina motorika). V veliko primerih k izboljšanju navedenih težav pripomore redna miselna in tudi fizična dejavnost na primernih vajah. Ker je otroke v splošnem težko motivirati v redno izvajanje suhoparnih vaj, je naš predlog, da bi preko mobilne igre spodbudili razvoj fine motorike in koncentracije. Tako bi na bolj zabaven način pokrili del potrebnih vadbenih dejavnosti.  

# 2. Opis igre #
Igro lahko kategoriziramo kot igro labirint. Osnovna naloga igralca je, da glavni lik preko labirinta 
pripelje na njegovo končni cilj.  
Labirint se v igri generira samodejno. Vsak uspešni prihod na cilj igralca nagradi z novo stopnjo, ki je 
za odtenek težja. Ker se igra generira samodejno, je tudi stopenj skoraj neomejeno – realno gledano 
toliko, kolikor je še na voljo zaslonskih pik na zaslonu. Težavnosti lahko nastavljamo z več 
mehanizmi. Od samega povečanja dolžine in širine labirintnega poligona, časovne omejitve igranja, 
do omejitve števila poskusov na isti stopnji.  
Ima pa igra tudi mehanizme za pomoč igralcu. Prvi je tak, ki ga določi ustvarjalec instance igre 
(najpogosteje je to učitelj). Preko nastavitev igre se lahko določi kolikšen odstotek poti ima na poti 
posebne oznake, ki pomagajo igralca usmerjati po pravi poti. Drugi mehanizem lahko izbere 
uporabnik sam. To je gumb, ki mu animirano izriše celotno pot od trenutnega položaja glavnega lika, 
do njegovega cilja. V tem primeru se igralcu ne šteje kot uspešno zaključena stopnja, vendar ga zgolj 
opogumi, da pot zares obstaja. Nadalje se generira nov poligon na isti stopnji, kjer lahko igralec 
ponovno poskusi z novo igro. 
Igra deluje v dveh grafičnih načinih. Prvi omogoča ustvariti labirint z lastno kombinacijo barv. Druga 
možnost pa je, da labirint ustvarimo z lastnimi slikami. Igra je zasnovana na način, kjer lahko vsaka 
instanca predstavlja drugo zgodbo. V nastavitvah namreč lahko določimo tako ime kot kratek 
motivacijski opis, ki se prikažeta ob prihodu igralca v igro. Med igranjem se na podlagi različnih akcij 
in stanj generirajo tudi predpripravljeni zvoki. V kombinaciji s tematskimi slikami tako poustvari 
svojevrsten igralski svet.  
Kot celota je igra, zasnovana zelo splošno, z veliko mogočimi nastavitvami in tako daje škarje in 
platno v roke ustvarjalcem instanc (učiteljem). Od njihove ustvarjalnosti je tako odvisno, kako 
zabavna je lahko igralna izkušnja. 
Igra je narejena s HTML5 in JavaScript tehnologijami. Integrirana je tudi na platformi ZaznajSpoznaj.  
 
 
 
# 3. Izobraževalni namen: #
Kot pravi stari rek, vaja dela mojstra, vendar mojster postane uspešen šele po veliko vaje. Ideja 
predlagane igre je na preprost in zabaven način motivirati slabovidne otroke k rednemu opravljanju 
gibalno miselnih vaj. Igra skozi vnaprej pripravljen scenarij pokrivala vaje tako za koordinacijo, 
motoriko in ne nazadnje koncentracijo. 
Scenariji se generirajo po stopnjah in glede na različne atribute kompleksnosti: 
-  Širina poti. Od bolj široke proti ožji - motivacija je k čim točnejšemu sledenju poti. 
-  Barve in kontrasti. Razločevanje barv in slik - nekateri otroci imajo težavo z razpoznavanjem 
barv in odtenkov. 
-  Dolžina poti/časovna zahtevnost. Utrjevanje koncentracije in motorike. 
Igra je v prvi vrsti namenjena slovenskim slabovidnim otrokom. Kot jezik igra podpira tudi 
angleščino – tako kot sama platforma ZaznajSpoznaj. Starostnih ali drugih omejitev tako ne tehnično 
ne vsebinsko ni. Z le nekaj spremembami v nastavitvah je igro mogoče preoblikovati in uporabiti tudi 
za druge namene. Ne nazadnje tudi v tako kjer bi še popolnoma zdravi odrasli osebi pritekla kakšna 
kaplja znoja po čelu. 
 
# 4. Zaslonski posnetki igre #
Na platformi ZaznajSpoznaj smo za demonstracijske namene ustvarili štiri, nekoliko hudomušne, prototipne zgodbe. 
   
   
Slika 1 prikazuje vstopno sliko razreda Najdi pot, na platformi ZaznajSpoznaj, z nekaj instancami iger. 
 ![prototip photo1] (https://drive.google.com/uc?id=103wZDSpkgi1g0AYGvIh6CmxG4L4-iW0d&export=download)
 
 

Slika 2 prikazuje našo prvo predstavljeno zgodbo, to je družina Novak in njihova pot do doma. Ob prihodu v igro se pojavi krajši opis zgodbe in namena igre.  
![prototip photo2] (https://drive.google.com/uc?id=1iPSya9LuZXSJbzttNl9BK2CqDAJnHnv_&export=download)
 

Slika 3 prikazuje igralni poligon za dotično zgodbo. 
![prototip photo3] (https://drive.google.com/uc?id=14OkTWI17kjo1jPZxdUO_3nsM3zBdHFlh&export=download)



Slika 4 
Ob uspešnem prihodu igralca do cilja se mu na zaslonu pojavi okno s čestitko ter informacijami o trenutni stopnji. To okno prikazuje slika Slika 4. 
 ![prototip photo4] (https://drive.google.com/uc?id=1yuTH3zojYx7xm8QHSj3aaaHo5xC6PRut&export=download)
 
 
 
 Slika 5 
V primeru, da se igralec naveliča trenutnega poligona ali mu poteče čas se pojavi okno na sliki Slika 5, ki motivira igralca v nadaljevanje z novo igro. 
![prototip photo5] (https://drive.google.com/uc?id=13MC9XmVDkomUJHdGU40ERSzJa2xMoTrI&export=download)



Slika 6 prikazuje mehanizem pomoči uporabniku. Uporabnik lahko s klikom na gumb za izris poti zaprosi igro, da mu pokaže celotno pot. Prikaz poti je lepo animiran. Igralcu se ta akcija ne šteje kot uspešen zaključek igre, zato se za tem generira nov poligon na isti stopnji. 
  ![prototip photo6] (https://drive.google.com/uc?id=1ch5IkkFKfE8sO_wBymWVkrCthotSpV7t&export=download)



Slika 7 predstavlja nova zgodbo. Tokrat o barvnih balončkih. 
  ![prototip photo7] (https://drive.google.com/uc?id=1S19sQ-POAklVp2KFXqmLipah0k-C-ygb&export=download)

 

 Slika 8 predstavlja igro, kjer je poligon generiran le s kombinacijo izbranih barv. Vijoličasti krogci pa predstavljajo mehanizem pomoči igralcu, saj ga usmerjajo po pravi poti do cilja. V konkretnem primeru so oznake za pomoč razporejene na deset odstotkov poti.  
 ![prototip photo8] (https://drive.google.com/uc?id=1zsYDHHWSLT0EuoCUi9GBs-ustehSotLk&export=download)


 Slika 9 predstavlja že našo tretjo zgodbo. 
 ![prototip photo9] (https://drive.google.com/uc?id=12blrLzqMfg_HLxYu1GUf9xgLcRK4yGey&export=download)



Slika 10 
Poligon na sliki Slika 10 je ponovno sestavljen iz slik. Začetna nastavitev stopnje pa je 13, kar pomeni začetniška. 
  ![prototip photo10] (https://drive.google.com/uc?id=1lu7Kh4BJtP_HYv5oS5JXN818oDrdzjWg&export=download)
 
  

 Slika 11 predstavlja našo zadnjo zgodbo, o malem pingvinu in njegovi sladkosnednosti. 
  ![prototip photo11] (https://drive.google.com/uc?id=14UMHo5h0cN2k4qm5B7UjoKMGL9mOGMce&export=download)

 

Slika 12 
Poligon iz slike Slika 12 je ponovno sestavljen iz slik. Vijoličasti krogci predstavljajo izbran mehanizem pomoči igralcu. V tem primeru so označbe pomoči postavljene po celotni dolžini poti do cilja. 
 ![prototip photo12] (https://drive.google.com/uc?id=13HKYyFOyCKZXgEZZmPcwrP_dURsbdfm1&export=download)



# Ključne lastnosti igre #
- tehnologije HTML5 in JavaScript
- algoritem A* za iskanje najkrajše poti (kot pomoč igralcu)
- izris najkrajše poti je lahko skozi vso dolžino ali precentualen delež poti (npr. samo dva odstotka, eno petino, itd.)
- lasten algoritem za naključen izris poligona
- poligonu lahko preko parametrov določimo poljubno število stolpcev in vrstic
- poligon se prilagaja velikosti zaslona
- modularna zasnova (lahko menjamo temo zgodbe, slike, stopnje/zahtevnost igre)
- deluje na klasičnih računalnikih z miško ali telefonih/tabliciah s prsti
- ..


(The code will be published soon, I need a little time to clean up all internal things, meantime you can contact me and hopefully I will provide you the code)